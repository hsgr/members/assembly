## Operator info

- EMail:  
- Username:  (In HSGR services)  
- Public Name:  (For website)  
- HSGR mail alias: whateveryoulike@hackerspace.gr  
- Matrix user:  
- GitLab User:  

## Physical access team

- [ ] Provide Digital door key
- [ ] Register Digital door key

## Financial team
- [ ] Inform the [HSGR Financial team](https://www.hackerspace.gr/wiki/Financial-team) about the membership tier level and the payment method.
- [ ] Hackerspace passport
## Ops team:

- [ ] @hackerspace.gr email alias
- [ ] Invite to HGSR Members Matrix/Element room
- [ ] Invite as developer in the [HSGR-Assembly](https://gitlab.com/hsgr/members/assembly/-/boards)
- [ ] Add to Members list on the HSGR website
- [ ] Add wiki user category [[Category:Members]]
- [ ] Add to Members mailing list 
- [ ] Add to HSGR LDAP

/confidential
/label ~members 